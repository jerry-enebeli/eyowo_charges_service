const jwt = require("jsonwebtoken");
const config = require("../config/config.js");

const User = require("../models/user");

module.exports.validateSession = (req, res, next) => {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (
    req.eyowo_user === undefined ||
    req.eyowo_user._id === undefined ||
    req.eyowo_user._id === null
  ) {
    return res.status(403).send({
      success: false,
      message: "Request unauthorized"
    });
  }

  if (req.params.user_id !== req.eyowo_user._id.toString()) {
    return res.status(403).send({
      success: false,
      message: "Request unauthorized; false header"
    });
  }
  // everything good, move on
  next();
};

module.exports.validateToken = (req, res, next) => {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: "No token provided."
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: "Invalid or no authorization token provided."
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: "Failed to authenticate token.",
        err
      });
    }
    User.findById(decoded._id, (errr, user) => {
      if (errr) {
        console.log("nothing found");
        return res.status(403).send({
          success: false,
          message: "Authentication failed. Eyowo user not found."
        });
      }
      // if everything is good, save to request for use in other routes
      req.token = decoded;
      req.eyowo_user = user;

      next();
    });
  });
};

module.exports.validateSetSecurePinToken = (req, res, next) => {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: "No token provided."
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: "Invalid or no authorization token provided."
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: "Failed to authenticate token.",
        err
      });
    }
    if (decoded.usage === undefined) {
      return res.status(403).send({
        success: false,
        message: "Invalid token rights."
      });
    }
    if (decoded.usage !== "setpin") {
      return res.status(403).send({
        success: false,
        message: "Token cannot perform this action."
      });
    }

    req.set_securepin_token = decoded;

    next();
  });
};
