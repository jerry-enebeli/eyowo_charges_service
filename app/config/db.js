const mongoose = require('mongoose');
require('dotenv').config();
mongoose.Promise = require('bluebird');
const options = { useMongoClient: true, keepAlive: 300000, connectTimeoutMS: 30000 };

const connections = () => {
  mongoose.connect(process.env.DB_HOST, options, (err) => {
    if (err) throw err;
    console.log('db connected');
  });
};

module.exports = connections;
