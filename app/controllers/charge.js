const Charge = require("../models/charge");

const CreateCharge = async (call, callback) => {
  try {
    const req = call.request;
    console.log(req.clients);
    req.clients = JSON.parse(req.clients);
    req.debtor = JSON.parse(req.debtor);
    const data = await Charge.create(req);
    if (data) {
      return callback(null, {
        success: true,
        data: JSON.stringify(data)
      });
    }
  } catch (err) {
    return callback(null, {
      success: false,
      data: JSON.stringify(err)
    });
  }
};

const GetCharge = async (call, callback) => {
  try {
    const req = call.request;
    const data = await Charge.findOne({
      code: req.code
    });
    if (data) {
      return callback(null, {
        success: true,
        data: JSON.stringify(data)
      });
    }
    return callback(null, {
      success: true,
      data: JSON.stringify({ message: "No Charge found" })
    });
  } catch (err) {
    return callback(null, {
      success: false,
      data: JSON.stringify(err)
    });
  }
};

const GetCharges = async (call, callback) => {
  try {
    const req = call.request;
    const data = await Charge.find({});
    if (data) {
      return callback(null, {
        success: true,
        data: JSON.stringify(data)
      });
    }
    return callback(null, {
      success: true,
      data: JSON.stringify({ message: "Charges not found" })
    });
  } catch (err) {
    return callback(null, {
      success: false,
      data: JSON.stringify(err)
    });
  }
};

const UpdateCharge = async (call, callback) => {
  try {
    const req = call.request;
    const data = await Charge.update(
      {
        code: req.code
      },
      {
        $set: JSON.parse(req.body)
      }
    );

    if (data.n > 0) {
      return callback(null, {
        success: true,
        data: JSON.stringify(data)
      });
    }
  } catch (err) {
    return callback(null, {
      success: false,
      data: JSON.stringify(err)
    });
  }
};

const DeleteCharge = async (call, callback) => {
  try {
    const req = call.request;
    const data = await Charge.remove({
      code: req.code
    });
    console.log(data);
    if (data.n > 0) {
      return callback(null, {
        success: true,
        data: JSON.stringify(data)
      });
    } else {
      return callback(null, { success: true, data: "Deleted Successfully" });
    }
  } catch (err) {
    return callback(null, {
      success: false,
      data: JSON.stringify(err)
    });
  }
};

const VerifyCharge = async (call, callback) => {
  try {
    const req = call.request;
    const data = await Charge.findOne({
      code: req.code
    });
    if (data) {
      return callback(null, {
        success: true,
        data: JSON.stringify({ message: "Charge exist in database" })
      });
    }
    return callback(null, {
      success: false,
      data: JSON.stringify({ message: "Charge does not exist" })
    });
  } catch (err) {
    return callback(null, {
      success: false,
      data: JSON.stringify(err)
    });
  }
};

function CreditStruct(name, amount, wallet) {
  this.name = name;
  this.amount = amount;
  this.wallet = wallet;
  this.type = "Credit";
}

function DebitStruct(name, amount, wallet) {
  this.name = name;
  this.amount = amount;
  this.wallet = wallet;
  this.type = "Debit";
}

const GenerateDebtor = (charge, data) => {
  console.log(charge);
  if (charge.debtor.wallet === "sender") {
    if (charge.debtor.type === "fixed") {
      let debtor = new DebitStruct(
        charge.name,
        data.amount + charge.debtor.amount,
        data.sender
      );
      return debtor;
    } else if (charge.debtor.type === "percent") {
      let debtor = new DebitStruct(
        charge.name,
        data.amount + charge.debtor.amount,
        data.sender
      );
      return debtor;
    }
  } else if (charge.debtor.wallet === "recipient") {
    let debtor = new DebitStruct(
      charge.name,
      data.amount + charge.debtor.amount * data.amount,
      data.recipient
    );

    return debtor;
  }
};

const Generatecredit = (charge, data) => {
  if (charge.wallet === "sender") {
    if (charge.type === "fixed") {
      let debtor = new CreditStruct(
        charge.name,
        charge.amount,
        data.sender
      );
      return debtor;
    } else if (charge.type === "percent") {
      let debtor = new CreditStruct(
        charge.name,
        data.amount * charge.amount,
        data.sender
      );
      return debtor;
    }
  } else if (charge.wallet === "recipient") {
    if (charge.type === "fixed") {
      let debtor = new CreditStruct(
        charge.name,
        charge.amount,
        data.recipient
      );
      return debtor;
    } else if (charge.type === "percent") {
      let debtor = new CreditStruct(
        charge.name,
        data.amount * charge.amount,
        data.recipient
      );
      return debtor;
    }
  } else {
    if (charge.type === "fixed") {
      let debtor = new CreditStruct(
        charge.name,
        charge.amount,
        charge.wallet
      );
      return debtor;
    } else if (charge.type === "percent") {
      let debtor = new CreditStruct(
        charge.name,
        data.amount * charge.amount,
        charge.wallet
      );
      return debtor;
    }
  }
};

const initCharge = async (code, action, data) => {
  const charge = await Charge.findOne({ code });
  if (!charge) {
    return new Promise(resolve => {
      return resolve(data);
    });
  }
  if (action === "debit") {
    return new Promise(resolve => {
      return resolve(GenerateDebtor(charge, data));
    });
  } else if (action === "credit") {
    let wallets = [];
    if (charge.clients.length >= 1) {
      return new Promise(resolve => {
        charge.clients.forEach(element => {
          element.name = charge.name;
          element.remark = data.remark;
          wallets.push(Generatecredit(element, data));
          if (wallets.length === charge.clients.length) {
            return resolve(wallets);
          }
        });
      });
    }
  }
};

const GenerateCharge = async (call, callback) => {
  const req = call.request;
  req.data = JSON.parse(req.data);
  const charge = await initCharge(req.code, req.action, req.data);
  return callback(null, { success: true, data: JSON.stringify(charge) });
};

module.exports = {
  GenerateCharge,
  CreateCharge,
  GetCharge,
  UpdateCharge,
  VerifyCharge,
  GetCharges,
  DeleteCharge
};
