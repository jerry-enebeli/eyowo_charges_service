const crypto = require('crypto');

const sha512 = function sha512(key, salt) {
  const hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
  hash.update(key);
  const value = hash.digest('hex');
  return {
    salt,
    passwordHash: value,
  };
};

exports.saltHashPassword = function saltHashPassword(securepin, phone) {
  const hashpin = sha512(securepin, securepin + phone);
  return hashpin.passwordHash;
};
