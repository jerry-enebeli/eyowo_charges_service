// const request = require('request');
const requestPromise = require('request-promise-native');


module.exports.postToURL = async function postToURL(requestOptions) {
  const response = await requestPromise(requestOptions);
  return response;
};

module.exports.getFromURL = async function getFromURL(requestOptions) {
  const response = await requestPromise(requestOptions);
  return response;
};
