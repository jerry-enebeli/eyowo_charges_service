const grpc = require('grpc');

const PRORTO_PATH = `${__dirname}/app/rpc/proto/charge.proto`;

const EyowoCharge = grpc.load(PRORTO_PATH).EyowoCharge;


const client = new EyowoCharge.Charge('127.0.0.1:50053', grpc.credentials.createInsecure());


    client.CreateCharge({
      name: 'softcom',
      code: 'ABC1119',
      platform:'mobile',
      clients: JSON.stringify([
        {
            wallet:'recipient',
            amount:100,
            type:'fixed'
        }
      ]),
      debtor:JSON.stringify({
          wallet: 'sender',
          amount: 0.01,
          type: 'percent'
      })
    }, (err, response) => {
      if(response){
            response.data = JSON.parse(response.data)
        console.log(response);
      }
      console.log(err)
      
    });


//     const data  = {
//         amount: 5000,
//         recipient: '234899224663',
//         sender: "2348141804018"
//     }

// client.GenerateCharge({code:'ABC1113', action: 'debit', data: JSON.stringify(data)}, (err, resp)=>{
//     console.log(err) 
//     resp.data  = JSON.parse(resp.data)
//     console.log(resp)
// })

