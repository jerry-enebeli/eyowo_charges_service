const connections = require("./app/config/db");


const chargectrl = require("./app/controllers/charge");

const grpc = require("grpc");

const PRORTO_PATH = `${__dirname}/app/rpc/proto/charge.proto`;

const EyowoCharge = grpc.load(PRORTO_PATH).EyowoCharge;

const server = new grpc.Server();

connections();

server.addService(EyowoCharge.Charge.service, chargectrl);

server.bind("0.0.0.0:50053", grpc.ServerCredentials.createInsecure());
server.start();

console.log({ status: "RUNNING....", service: "Eyowo Charges Service" });
